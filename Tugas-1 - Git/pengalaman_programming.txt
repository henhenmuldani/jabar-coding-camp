Pengalaman Programming
1. Kuliah: Saya kira masuk jurusan Ilmu Komputer bakal banyak belajar hardware komputer, tetapi 
           kenyataanya tidak, malah banyak belajar programming. Walaupun kurang suka akhirnya terpaksa
           demi nilai belajar programming C, C#, Java, PHP, sampai akhirnya beres skripsi 
           dengan bahasa Java di Android Native.
2. Kerja: Karena terpaksa lagi dan BU (Butuh Uang) untuk mempertahankan hidup, saya coba lamar dengan
          skill Android Native yg didapat ketika mengerjakan skripsi, dan alhamdulillah dapat pekerjaan
          sebagai Android Developer. Setelah 1 tahun lebih dan merasa jenuh, saya coba pindah kantor
          namun bukan sebagai Android Developer melainkan sebagai RPA Developer. Setelah 1 Tahun lebih 
          menjadi RPA Developer saya coba daftar CPNS di daerah agar saya bisa berbakti kepada Orang Tua.
          Alhamdulillah loloslah tahun ini sebagai CPNS dan di pemerintahan pasti lebih butuh web daripada app Android
          maka saya mulai belajar web kembali dengan ikut Jabar Coding Camp.