//soal 2
/* 
Setelah no.1 berhasil, implementasikan function readBooks yang menggunakan callback 
di atas namun sekarang menggunakan Promise. 
Buatlah sebuah file dengan nama promise.js. 
Tulislah sebuah function dengan nama readBooksPromise 
yang me-return sebuah promise seperti berikut:
 
-di file promise.js
function readBooksPromise (time, book) {
  console.log(`saya mulai membaca ${book.name}`)
  return new Promise( function (resolve, reject){
    setTimeout(function(){
      let sisaWaktu = time - book.timeSpent
      if(sisaWaktu >= 0 ){
          console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
          resolve(sisaWaktu)
      } else {
          console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
          reject(sisaWaktu)
      }
    }, book.timeSpent)
  })
}
 
module.exports = readBooksPromise
 
Masih di folder yang sama dengan promise.js, 
buatlah sebuah file dengan nama index2.js. Tuliskan code sebagai berikut
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
Lanjutkan code untuk menjalankan function readBooksPromise 
Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) 
untuk membaca semua buku dalam array books.!

Petunjuk
Untuk mengerjakan soal di atas , tidak perlu dilooping,
cukup panggil satu satu elemen tiap array nya : books[0] , books[1] , dst.
Gunakan sisa waktu membaca books[0] untuk membaca books[1], 
kemudaian sisa waktu membaca books[1] untuk membaca books[2], dst.

*/
//jawaban soal 2
var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

function bacaBuku() {
  readBooksPromise(10000, books[0])
    .then((time) => {
      //console.log(time);
      return readBooksPromise(time, books[1]);
    })
    .then((time) => {
      //console.log(time);
      return readBooksPromise(time, books[2]);
    })
    .then((time) => {
      //console.log(time);
      return readBooksPromise(time, books[3]);
    })
    .catch(() => {
      console.log("Waktu Habis");
    });
}

bacaBuku();
