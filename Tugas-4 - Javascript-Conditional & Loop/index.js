//soal 1
/* 
buatlah variabel seperti di bawah ini
var nilai;
 
pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
nilai >= 85 indeksnya A
nilai >= 75 dan nilai < 85 indeksnya B
nilai >= 65 dan nilai < 75 indeksnya c
nilai >= 55 dan nilai < 65 indeksnya D
nilai < 55 indeksnya E
*/

//jawaban soal 1
var nilai;
nilai = 54;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("D");
} else {
  console.log("E");
}

//soal 2
/*
buatlah variabel seperti di bawah ini
var tanggal = 22;
var bulan = 7;
var tahun = 2020;
ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan 
buatlah switch case pada bulan, 
lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 
(isi di sesuaikan dengan tanggal lahir masing-masing)
*/

//jawaban soal 2
var tanggal = 17;
var bulan = 6;
var tahun = 1996;

switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
  default:
    bulan = "Bulan tidak ada";
    break;
}

var output = tanggal + " " + bulan + " " + tahun;
console.log(output);

//soal 3
/* 
Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).
Output untuk n=3 :
 
#
##
###
 
Output untuk n=7 :
 
#
##
###
####
#####
######
#######
*/

//jawaban soal 3
var n = 3;
var i = 0;

for (i = 0; i < n; i++) {
  var j = 0;
  var k = "";
  for (j = 0; j < i + 1; j++) {
    k = k + "#";
  }
  console.log(k);
}

//soal 4
/* 
berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
contoh :
Output untuk m = 3
1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
Output untuk m = 5
1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
Output untuk m = 7
1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS
======
7 - I love programming
Output untuk m = 10
1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS
======
7 - I love programming
8 - I love Javascript
9 - I love VueJS
=========
10 - I love programming
*/

//jawaban soal 4

var m = 10;
var i = 0;

for (i = 1; i <= m; i++) {
  if (i % 3 == 1) {
    console.log(i + " - I love programming");
  } else if (i % 3 == 2) {
    console.log(i + " - I love Javascript");
  } else {
    console.log(i + " - I love VueJS");
    var j = 1;
    var k = "";
    for (j = 1; j < i + 1; j++) {
      k = k + "=";
    }
    console.log(k);
  }
}
