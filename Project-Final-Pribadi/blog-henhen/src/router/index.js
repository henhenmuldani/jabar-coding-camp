import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/HomeView.vue"),
  },
  {
    path: "/blogs",
    name: "blogs",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/BlogsView.vue"),
  },
  {
    path: "/blog/:id",
    name: "blog",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/BlogView.vue"),
  },
  {
    path: "/add",
    name: "add",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AddBlogView.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
