//soal 1
/*
Function Penghitung Jumlah Kata

Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

Contoh
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4

catatan
Perhatikan double spasi di depan, belakang, maupun di tengah tengah kalimat
*/

//jawaban soal 1
function jumlah_kata(kalimat) {
  return console.log(kalimat.trim().split(" ").length);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = " Saya Iqbal";
var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2
jumlah_kata(kalimat_3); // 4

// console.log(
//   jumlah_kata(kalimat_1),
//   jumlah_kata(kalimat_2),
//   jumlah_kata(kalimat_3)
// );

//soal 2
/* 
Function Penghasil Tanggal Hari Esok
Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

contoh 1
var tanggal = 29
var bulan = 2
var tahun = 2020
next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

contoh 2
var tanggal = 28
var bulan = 2
var tahun = 2021
next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

contoh 3
var tanggal = 31
var bulan = 12
var tahun = 2020
next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

Catatan :
1. Dilarang menggunakan new Date()
2. Perhatikan tahun kabisat
*/

//jawaban soal 2

function next_date(tanggal, bulan, tahun) {
  tanggal = tanggal + 1;
  bulan = bulan;
  tahun = tahun;

  if (tahun % 4 == 0) {
    if (tanggal > 29) {
      tanggal = 1;
      bulan = bulan + 1;
      if (bulan > 12) {
        bulan = 1;
        tahun = tahun + 1;
      }
    }
  } else {
    if (tanggal > 28) {
      tanggal = 1;
      bulan = bulan + 1;
      if (bulan > 12) {
        bulan = 1;
        tahun = tahun + 1;
      }
    }
  }

  switch (bulan) {
    case 1:
      bulan = "Januari";
      break;
    case 2:
      bulan = "Februari";
      break;
    case 3:
      bulan = "Maret";
      break;
    case 4:
      bulan = "April";
      break;
    case 5:
      bulan = "Mei";
      break;
    case 6:
      bulan = "Juni";
      break;
    case 7:
      bulan = "Juli";
      break;
    case 8:
      bulan = "Agustus";
      break;
    case 9:
      bulan = "September";
      break;
    case 10:
      bulan = "Oktober";
      break;
    case 11:
      bulan = "November";
      break;
    case 12:
      bulan = "Desember";
      break;
    default:
      bulan = "Bulan tidak ada";
      break;
  }

  return console.log(tanggal + " " + bulan + " " + tahun);
}

//contoh 1
var tanggal = 29;
var bulan = 2;
var tahun = 2020;
next_date(tanggal, bulan, tahun); // output : 1 Maret 2020

//contoh 2
var tanggal = 28;
var bulan = 2;
var tahun = 2021;
next_date(tanggal, bulan, tahun); // output : 1 Maret 2021

//contoh 3
var tanggal = 31;
var bulan = 12;
var tahun = 2020;
next_date(tanggal, bulan, tahun); // output : 1 Januari 2021
